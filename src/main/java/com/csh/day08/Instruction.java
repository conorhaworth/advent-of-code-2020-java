package com.csh.day08;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public class Instruction {
    @Setter private Operation operation;
    private final int argument;
    @Setter
    private boolean visited;

    protected Instruction clone() {
        return new Instruction(operation, argument, visited);
    }
}
