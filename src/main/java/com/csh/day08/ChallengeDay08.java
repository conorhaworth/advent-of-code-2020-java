package com.csh.day08;

import com.csh.FileReader;
import com.csh.aspect.Reportable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ChallengeDay08 {
    private int accumulator;

    @Reportable
    public int part1() throws IOException {
        run(getInstructions());
        return accumulator;
    }

    @Reportable
    public int part2() throws IOException {
        var counter = 0;
        List<Instruction> instructions = getInstructions();
        for (Instruction instruction : instructions) {
            List<Instruction> instructionsCopy = copyInstructions(instructions);
            if (instruction.getOperation() == Operation.JMP) {
                instructionsCopy.get(counter).setOperation(Operation.NOP);
            } else if (instruction.getOperation() == Operation.NOP) {
                instructionsCopy.get(counter).setOperation(Operation.JMP);
            }

            boolean validRun = false;
            if(instruction.getOperation() != Operation.ACC) {
                validRun = run(instructionsCopy);
            }

            if (validRun) {
                break;
            }

            counter++;
        }
        return accumulator;
    }

    private boolean run(List<Instruction> instructionSet) {
        accumulator = 0;
        boolean exit = false;
        boolean validExecution = true;
        int counter = 0;
        while(!exit) {
            var currentInstruction = instructionSet.get(counter);
            if (currentInstruction.isVisited()) {
                validExecution = false;
                break;
            }
            currentInstruction.setVisited(true);
            switch(currentInstruction.getOperation()) {
                case ACC -> { accumulator += currentInstruction.getArgument(); counter++; }
                case JMP -> counter += currentInstruction.getArgument();
                case NOP -> counter++;
            }
            if (counter > instructionSet.size()-1) {
                exit = true;
            }
        }
        return validExecution;
    }

    private List<Instruction> getInstructions() throws IOException {
        return FileReader.readFile("day08.txt").stream()
                .map(rawInstruction -> {
                    String[] splitInstruction = rawInstruction.split(" ");
                    return new Instruction(Operation.valueOf(splitInstruction[0].toUpperCase()), Integer.parseInt(splitInstruction[1]), false);
                }).collect(Collectors.toList());
    }

    private List<Instruction> copyInstructions(List<Instruction> instructionsToCopy) {
        List<Instruction> instructionsCopy = new ArrayList<>();
        for (Instruction instruction : instructionsToCopy) {
            instructionsCopy.add(instruction.clone());
        }
        return instructionsCopy;
    }
}
