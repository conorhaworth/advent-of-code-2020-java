package com.csh.day07;

import com.csh.FileReader;
import com.csh.aspect.Reportable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

@Component
public class ChallengeDay07 {
    record Rule(String bag, List<Bag> holds) {}
    record Bag(int amount, String name){}

    private final Map<String, Rule> ruleMap = new HashMap<>();
    private final Set<String> bagTypes = new HashSet<>();

    @PostConstruct
    public void init() throws IOException {
        List<String> rules = FileReader.readFile("day07.txt");
        processRuleInput(rules);
    }

    @Reportable
    public int part1() throws IOException {
        var total = 0;
        for (String bagType : bagTypes) {
            if (!bagType.equals("shiny gold")) {
                if(canHoldGoldBag(bagType, ruleMap)) {
                    total++;
                }
            }
        }
        return total;
    }

    @Reportable
    public int part2() {
        Rule shinyGold = ruleMap.get("shiny gold");
        var total = 0;
        for (Bag bag : shinyGold.holds) {
            total += bag.amount + (bag.amount * countChildBags(ruleMap.get(bag.name), 0));
        }
        return total;
    }
    //1193236164 - too high

    private long countChildBags(Rule parent, long total) {
        for (Bag bag : parent.holds) {
            if (ruleMap.containsKey(bag.name)) {
                Rule rule = ruleMap.get(bag.name);
                if (rule.holds.isEmpty()) {
                    total += bag.amount;
                } else {
                    total += bag.amount + (bag.amount * countChildBags(rule, total));
                }
            }
        }
        return total;
    }


    private boolean canHoldGoldBag(String bagType, Map<String, Rule> ruleMap) {
        boolean canHold = false;
        if (ruleMap.containsKey(bagType)) {
            Rule rule = ruleMap.get(bagType);
            long doesHoldGold = rule.holds.stream().filter(bag -> bag.name.equals("shiny gold")).count();
            if (doesHoldGold > 0) {
                canHold = true;
            } else {
                for (Bag bag : rule.holds) {
                    canHold = canHoldGoldBag(bag.name, ruleMap);
                    if (canHold) {
                        break;
                    }
                }
            }
        }
        return canHold;
    }

    private String getFormattedBagName(String rawName) {
        var bagName = rawName.trim().replace(".", "");
        return bagName.substring(0, bagName.lastIndexOf(" "));
    }

    private void processRuleInput(List<String> rules) {
        for (String rule : rules) {
            String[] sections = rule.split("contain");
            List<Bag> bags = new ArrayList<>();
            if (!sections[1].trim().equals("no other bags.")) {
                List<String> holds = new ArrayList<>(Arrays.asList(sections[1].split(",")));
                for (String hold : holds) {
                    String[] bagValues = hold.trim().split(" ", 2);
                    bags.add(new Bag(Integer.parseInt(bagValues[0]), getFormattedBagName(bagValues[1])));
                }
            }

            Rule bagRule = new Rule(getFormattedBagName(sections[0]), bags);
            ruleMap.putIfAbsent(bagRule.bag, bagRule);

            bagTypes.add(bagRule.bag);
        }
    }
}
