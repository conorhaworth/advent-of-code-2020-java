package com.csh.day04;

import com.csh.Challenge;
import com.csh.FileReader;
import com.csh.aspect.Reportable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class ChallengeDay04 {
    private static final List<String> REQUIRED_FIELDS = Arrays.asList("byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:");
    private static final Pattern BYR_PATTERN = Pattern.compile("(byr:(19[2-9][0-9]|200[0-2]))");
    private static final Pattern IYR_PATTERN = Pattern.compile("(iyr:(201[0-9]|2020))");
    private static final Pattern EYR_PATTERN = Pattern.compile("(eyr:(202[0-9]|2030))");
    private static final Pattern HGT_CM_PATTERN = Pattern.compile("(hgt:((1[5-8][0-9]|19[0-3])cm))");
    private static final Pattern HGT_IN_PATTERN = Pattern.compile("(hgt:((59|6[0-9]|7[0-6])in))");
    private static final Pattern HCL_PATTERN = Pattern.compile("(hcl:#[0-9a-f]{6})");
    private static final Pattern ECL_PATTERN = Pattern.compile("(ecl:(amb|blu|brn|gry|grn|hzl|oth))");
    private static final Pattern PID_PATTERN = Pattern.compile("(pid:[0-9]{9})");
    private static final Pattern CID_PATTERN = Pattern.compile("(cid:.*)");
    private static final List<Pattern> PATTERNS = Arrays.asList(BYR_PATTERN, IYR_PATTERN, EYR_PATTERN, HGT_CM_PATTERN,
            HGT_IN_PATTERN, HCL_PATTERN, ECL_PATTERN, PID_PATTERN, CID_PATTERN);

    private List<String> passports;

    @PostConstruct
    public void init() {
        String[] input = FileReader.readFileSplitByEmptyLine("day04.txt");
        passports = Arrays.stream(input)
                .map(line -> line.replaceAll("\n", " "))
                .collect(Collectors.toList());
    }

    @Reportable
    public Long part1() {
        return passports.stream().filter(this::hasRequiredFields).count();
    }

    @Reportable
    public Long part2() {
        return passports.stream().filter(this::isValid).count();
    }

    private boolean hasRequiredFields(String passport) {
        return REQUIRED_FIELDS.stream().allMatch(passport::contains);
    }

    private boolean isValid(String passport) {
        String[] fields = passport.split(" ");
        return hasRequiredFields(passport) && Arrays.stream(fields)
                .allMatch(field -> PATTERNS.stream().anyMatch(pattern -> pattern.matcher(field).matches()));
    }
}
