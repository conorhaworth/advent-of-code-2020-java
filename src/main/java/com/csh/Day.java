package com.csh;

public abstract class Day {
    public abstract Object part1();
    public abstract Object part2();
}
