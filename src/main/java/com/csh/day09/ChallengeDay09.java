package com.csh.day09;

import com.csh.FileReader;
import com.csh.aspect.Reportable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ChallengeDay09 {
    private List<Long> xmas;

    @PostConstruct
    public void init() throws IOException {
         xmas = FileReader.readFile("day09.txt").stream()
                .mapToLong(Long::parseLong)
                .boxed()
                .collect(Collectors.toList());
    }

    @Reportable
    public long part1() {
        return getInvalidNumber(25, xmas);
    }

    @Reportable
    public long part2() {
        long invalidNumber = getInvalidNumber(25, xmas);
        return getWeakness(invalidNumber);
    }

    private Long getWeakness(long invalidNumber) {
        Set<Long> set = new HashSet<>();
        for (int i = 0; i < xmas.size(); i++) {
            set = findContiguousSetForTarget(invalidNumber, i);
            if (!set.isEmpty()) {
                break;
            }
        }
        Optional<Long> min = set.stream().min(Long::compareTo);
        Optional<Long> max = set.stream().max(Long::compareTo);
        long weakness = 0;
        if (min.isPresent()) {
            weakness = min.get() + max.get();
        }
        return weakness;
    }

    private Set<Long> findContiguousSetForTarget(long target, int startIndex) {
        boolean found = false;
        int counter = 0;
        long total = 0;
        Set<Long> set = new HashSet<>();
        while (!found) {
            long currentNum = xmas.get(startIndex + counter);
            set.add(currentNum);
            total += currentNum;
            if (total == target) {
                found = true;
            } else if (total > target) {
                break;
            } else {
                counter++;
            }
        }
        if (found) {
            return set;
        } else {
            return Set.of();
        }
    }

    private long getInvalidNumber(int preambleSize, List<Long> xmas) {
        long weakness = 0;
        for (int i = preambleSize; i < xmas.size(); i++) {
            List<Long> preamble = xmas.subList(i - preambleSize, i);
            if (!isValid(xmas.get(i), preamble)) {
                weakness = xmas.get(i);
                break;
            }
        }
        return weakness;
    }

    private boolean isValid(long target, List<Long> preamble) {
        boolean valid = false;
        for (int i = 0; i < preamble.size(); i++) {
            long firstNum = preamble.get(i);
            for (int j = 0; j < preamble.size(); j++) {
                long secondNum;
                int nextIndex = i + j;
                if (nextIndex >= preamble.size()) {
                    secondNum = preamble.get(nextIndex % preamble.size());
                } else {
                    secondNum = preamble.get(nextIndex);
                }
                if (firstNum != secondNum && (firstNum + secondNum == target)) {
                    valid = true;
                    break;
                }
            }
            if (valid) {
                break;
            }
        }
        return valid;
    }

}
