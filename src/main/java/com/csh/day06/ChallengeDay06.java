package com.csh.day06;

import com.csh.FileReader;
import com.csh.aspect.Reportable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class ChallengeDay06 {
    private String[] input;
    private int part1Total;
    private int part2Total;

    @PostConstruct
    public void init() {
        input = FileReader.readFileSplitByEmptyLine("day06.txt");
    }

    @Reportable
    public int part1() {
        processAnswers();
        return part1Total;
    }

    @Reportable
    public int part2() {
        return part2Total;
    }

    private void processAnswers() {
        for (String group : input) {
            var personAnswers = group.split("\n");
            var groupAnswers = mapAnswers(personAnswers);
            part1Total += groupAnswers.size();
            part2Total += groupAnswers.entrySet().stream().filter(answer -> answer.getValue() == personAnswers.length).count();
        }
    }

    private Map<String, Integer> mapAnswers(String[] personAnswers) {
        Map<String, Integer> groupAnswers = new HashMap<>();
        for (String personAnswer : personAnswers) {
            for (String answer : personAnswer.split("")) {
                groupAnswers.computeIfPresent(answer, (key, value) -> ++value);
                groupAnswers.putIfAbsent(answer, 1);
            }
        }
        return groupAnswers;
    }
}
