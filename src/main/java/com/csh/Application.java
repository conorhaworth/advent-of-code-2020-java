package com.csh;

import com.csh.config.ApplicationConfig;
import com.csh.day01.ChallengeDay01;
import com.csh.day02.ChallengeDay02;
import com.csh.day03.ChallengeDay03;
import com.csh.day04.ChallengeDay04;
import com.csh.day05.ChallengeDay05;
import com.csh.day06.ChallengeDay06;
import com.csh.day07.ChallengeDay07;
import com.csh.day08.ChallengeDay08;
import com.csh.day09.ChallengeDay09;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class Application {

    public static void main(String[] args) throws IOException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        ChallengeDay09 day09 = context.getBean(ChallengeDay09.class);
        day09.part1();
        day09.part2();
//        runAll(context);
    }

    public static void runAll(ApplicationContext context) {
        ChallengeDay01 day01 = context.getBean(ChallengeDay01.class);
        ChallengeDay02 day02 = context.getBean(ChallengeDay02.class);
        ChallengeDay03 day03 = context.getBean(ChallengeDay03.class);
        ChallengeDay04 day04 = context.getBean(ChallengeDay04.class);
        ChallengeDay05 day05 = context.getBean(ChallengeDay05.class);
        ChallengeDay06 day06 = context.getBean(ChallengeDay06.class);
        day01.part1();
        day01.part2();
        day02.part1();
        day02.part2();
        day03.part1();
        day03.part2();
        day04.part1();
        day04.part2();
        day05.part1();
        day05.part2();
        day06.part1();
        day06.part2();
    }
}
