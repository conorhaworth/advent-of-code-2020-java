package com.csh.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ReportAspect {
    @Pointcut("@annotation(Reportable)")
    public void executeReporting() {
    }

    @Around("executeReporting()")
    public Object incrementCount(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();

        Object returnValue = joinPoint.proceed();

        long totalTime = System.currentTimeMillis() - startTime;
        String className = joinPoint.getSignature().getDeclaringTypeName().split("\\.")[3];
        String methodName = joinPoint.getSignature().getName();
        System.out.println("###############################################################");
        System.out.println("Running " + className + " " + methodName);
        System.out.println("Answer: " + returnValue);
        System.out.println("Runtime: " + totalTime + "ms");
        System.out.println("###############################################################");
        System.out.println();


        return returnValue;
    }
}
