package com.csh;

import org.springframework.stereotype.Component;

public interface Challenge<T> {
    T part1();
    T part2();
}
