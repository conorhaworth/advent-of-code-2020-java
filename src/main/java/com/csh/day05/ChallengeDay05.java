package com.csh.day05;

import com.csh.FileReader;
import com.csh.aspect.Reportable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Component
public class ChallengeDay05 {
    private List<String> boardingPasses;

    @PostConstruct
    public void init() throws IOException {
        boardingPasses = FileReader.readFile("day05.txt");
    }

    @Reportable
    public Double part1() {
        var seatIds = getSeatIds();
        Optional<Double> max = seatIds.stream().max(Double::compareTo);
        return max.orElse(0d);
    }

    @Reportable
    public Double part2() {
        var seatIds = getSeatIds();
        seatIds.sort(Double::compareTo);
        double missingSeatId = 0d;
        for (int i = 0; i < seatIds.size(); i++) {
            Double seatId = seatIds.get(i);
            if (i+1 < seatIds.size()) {
                Double nextSeatId = seatIds.get(i+1);
                if (seatId+2 == nextSeatId) {
                    missingSeatId = seatId+1;
                }
            }
        }
        return missingSeatId;
    }

    private List<Double> getSeatIds() {
        List<Double> seatIds = new ArrayList<>();
        for (String boardingPass : boardingPasses) {
            double row = binarySearch(boardingPass.substring(0, 7).split(""), 127, 0, "B", "F");
            double column = binarySearch(boardingPass.substring(7).split(""), 7, 0, "R", "L");
            double seatId = (row * 8) + column;
            seatIds.add(seatId);
        }
        return seatIds;
    }

    private double binarySearch(String[] characters, double upperBound, double lowerBound, String upperRegion, String lowerRegion) {
        for (String character : characters) {
            if (character.equals(lowerRegion)) {
                upperBound = Math.floor((upperBound + lowerBound) / 2);
            }
            if (character.equals(upperRegion)) {
                lowerBound = Math.ceil((upperBound + lowerBound) / 2);
            }
        }
        return characters[characters.length-1].equals(lowerRegion) ? lowerBound : upperBound;
    }
}
