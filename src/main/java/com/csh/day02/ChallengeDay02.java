package com.csh.day02;

import com.csh.Challenge;
import com.csh.FileReader;
import com.csh.aspect.Reportable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ChallengeDay02 {
    public record PasswordPolicy(int lowerBound, int upperBound, String letter, String password) {}

    private List<PasswordPolicy> passwordPolicies;

    @PostConstruct
    public void init() throws IOException {
        passwordPolicies = getPasswordPolicies();
    }

    @Reportable
    public Integer part1() {
        int validPasswords = 0;
        for (PasswordPolicy passwordPolicy : passwordPolicies) {
            int count = 0;
            for (String character : passwordPolicy.password.split("")) {
                if (character.equals(passwordPolicy.letter)) {
                    count++;
                }
            }
            if (count >= passwordPolicy.lowerBound && count <= passwordPolicy.upperBound) {
                validPasswords++;
            }
        }
        return validPasswords;
    }

    @Reportable
    public Integer part2() {
        int validPasswords = 0;
        for (PasswordPolicy passwordPolicy : passwordPolicies) {
            char firstChar = passwordPolicy.password.charAt(passwordPolicy.lowerBound - 1);
            char secondChar = passwordPolicy.password.charAt(passwordPolicy.upperBound - 1);

            if (firstChar == passwordPolicy.letter.toCharArray()[0] && !(secondChar == passwordPolicy.letter.toCharArray()[0])) {
                validPasswords++;
            }
            if (!(firstChar == passwordPolicy.letter.toCharArray()[0]) && secondChar == passwordPolicy.letter.toCharArray()[0]) {
                validPasswords++;
            }
        }
        return validPasswords;
    }

    private List<PasswordPolicy> getPasswordPolicies() throws IOException {
        List<String> input = FileReader.readFile("day02.txt");
        return input.stream().map(rawInput -> {
            String[] sections = rawInput.split(" ");
            String[] ranges = sections[0].split("-");
            return new PasswordPolicy(Integer.parseInt(ranges[0]), Integer.parseInt(ranges[1]), sections[1].split("")[0], sections[2]);
        }).collect(Collectors.toList());
    }
}
