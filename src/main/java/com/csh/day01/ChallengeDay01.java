package com.csh.day01;

import com.csh.Challenge;
import com.csh.FileReader;
import com.csh.aspect.Reportable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;

@Component
public class ChallengeDay01 {
    private List<String> input;

    @PostConstruct
    public void init() throws IOException {
        input = FileReader.readFile("day01.txt");
    }

    @Reportable
    public Integer part1() {
        int answer = 0;
        for (int i = 0; i < input.size(); i++) {
            Integer entry1 = Integer.valueOf(input.get(i));
            for (int j = 0; j < input.size(); j++) {
                Integer entry2 = Integer.valueOf(input.get(j));
                if (i != j && (entry1 + entry2 == 2020)) {
                    answer = entry1 * entry2;
                    return answer;
                }
            }
        }
        return answer;
    }

    @Reportable
    public Integer part2() {
        int answer = 0;
        for (int i = 0; i < input.size(); i++) {
            Integer entry1 = Integer.valueOf(input.get(i));
            for (int j = 0; j < input.size(); j++) {
                Integer entry2 = Integer.valueOf(input.get(j));
                for (int k = 0; k < input.size(); k++) {
                    Integer entry3 = Integer.valueOf(input.get(k));
                    if (i != j && i != k && j != k && (entry1 + entry2 + entry3 == 2020)) {
                        answer = entry1 * entry2 * entry3;
                        return answer;
                    }
                }
            }
        }
        return answer;
    }
}
