package com.csh;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class FileReader {

    public static List<String> readFile(String filename) throws IOException {
        return Files.lines(Paths.get("src/main/resources/" + filename)).collect(Collectors.toList());
    }

    public static String[] readFileSplitByEmptyLine(String filename) {
        String input = "";
        try {
            input = new Scanner(new File("src/main/resources/" + filename)).useDelimiter("\\Z").next();
        } catch (FileNotFoundException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
        return input.split("\n\n");
    }
}
