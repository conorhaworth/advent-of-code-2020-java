package com.csh.structure.tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Node<T> {
    private final T value;
    private final List<Node<T>> children;

    public Node(T value) {
        this.value = value;
        this.children = new ArrayList<>();
    }

    public void addChild(Node<T> child) {
        children.add(child);
    }

    public void addChildren(Node<T>... newChildren) {
        children.addAll(Arrays.asList(newChildren));
    }

    public T getValue() {
        return value;
    }

    public List<Node<T>> getChildren() {
        return Collections.unmodifiableList(children);
    }
}
