package com.csh.day03;

import com.csh.Challenge;
import com.csh.FileReader;
import com.csh.aspect.Reportable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;

@Component
public class ChallengeDay03 {
    private List<String> input;

    @PostConstruct
    public void init() throws IOException {
        input = FileReader.readFile("day03.txt");
    }

    @Reportable
    public Integer part1() {
//        displayGrid();
        return findCollisions(input, 3, 1);
    }

    @Reportable
    public Integer part2() {
        int[][] tries = {{1,1}, {3,1}, {5,1}, {7,1}, {1,2}};
        var total = 1;
        for (int[] t : tries) {
            var trees = findCollisions(input, t[0], t[1]);
             total *= trees;
        }
//        displayGrid();
        return total;
    }

    private int findCollisions(List<String> grid, int right, int down) {
        int trees = 0;
        int currentX = 0;
        int currentY = 0;

        while (currentY < grid.size()) {
            var line = grid.get(currentY);
            if (currentX >= line.length()) {
                currentX = currentX - line.length();
            }
            if (line.charAt(currentX) == '#') {
                trees++;
            }
            currentX += right;
            currentY += down;
        }

        return trees;
    }

    private void displayGrid() throws IOException {
        var grid = createGrid();
        for (String[] strings : grid) {
            for (String string : strings) {
                System.out.print(string);
            }
            System.out.println();
        }
    }

    private String[][] createGrid() throws IOException {
        List<String> input = FileReader.readFile("day03.txt");
        String[][] grid = new String[input.size()][input.get(0).length()];
        for (int i = 0; i < input.size(); i++) {
            String[] line = input.get(i).split("");
            System.arraycopy(line, 0, grid[i], 0, line.length);
        }
        return grid;
    }
}
